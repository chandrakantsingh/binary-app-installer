#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

import subprocess
import threading
import time
import filters

cancelButton  = Gtk.Button('Close')
cancelButton.set_size_request(100, -1)
installButton  = Gtk.Button('Install')
installButton.set_size_request(100, -1)

class Buttons(Gtk.HBox):
	def __init__(self, textentry, the_buffer):
		Gtk.HBox.__init__(self, spacing=5)
		self.textentry = textentry
		self.buffer = the_buffer

		self.innerBox = Gtk.Grid()
		self.buttonBox = Gtk.HBox(spacing=5)

		self.buttonBox.pack_end(cancelButton, False, False, 0)
		self.buttonBox.pack_end(installButton, False, False, 0)

		self.innerBox.attach(self.buttonBox, 1, 0, 1, 1)
		self.buttonBox.set_hexpand(True)

		self.pack_start(self.innerBox, True, True, 0)

		installButton.connect('clicked', self.start_installation)
		cancelButton.connect('clicked', Gtk.main_quit)

	def start_installation(self, btn):
		if self.textentry.get_text() == '' or self.textentry.get_text() == None:
			self.show_message('error', 'Installation error', 'Please select the downloaded package before clicking Install.')
			return
		script = self.textentry.get_text()
		if script == None:
			self.show_message('error', 'Installation error', 'Dont know how to install the downloaded package.\nPlease let us know about it by mailing us: package@aryalinux.info')
			return
		self.proc = subprocess.Popen(
			filters.get_script(script) + ' ' + script,
			stdout = subprocess.PIPE,
			stderr = subprocess.PIPE,
			universal_newlines=True,
			shell=True
		)
		self.buffer.bind_subprocess(self.proc)
		btn.set_sensitive(False)

	def cancel_installation(self, btn):
		if self.proc == None:
			return
		poll = self.proc.poll()
		if poll == None:
			dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, "Cancel Installation?")
			dialog.format_secondary_text("Are you sure you want to cancel the installation?")
			response = dialog.run()
			if response == Gtk.ResponseType.NO:
				dialog.destroy()
				return
		if len(self.buffer.IO_WATCH_ID):
			for id_ in self.buffer.IO_WATCH_ID:
				GObject.source_remove(id_)
			self.buffer.IO_WATCH_ID = tuple()
			self.proc.terminate()
			return

	def show_message(self, msg_type, heading, message):
		if msg_type == 'info':
			dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.OK, heading)
		elif msg_type == 'error':
			dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.CANCEL, heading)
		dialog.format_secondary_text(message)
		dialog.run()
		dialog.destroy()

