#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from gi.repository import Gdk
from streamtextbuffer import StreamTextBuffer
from buttons import Buttons
import subprocess
 
result = subprocess.check_output('xrandr | grep "*"', shell=True)

width, height = result.decode().split()[0].split('x')

class Installer(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="AryaLinux Binary App Installer")

		self.the_box = Gtk.VBox()
		self.browser_box = Gtk.HBox()

		self.label = Gtk.Label(label='Browse for the file you have downloaded:', xalign=0)
		self.progress_label = Gtk.Label(label='Progress:', xalign=0)
		self.file_path = Gtk.Entry()
		self.file_path.set_hexpand(True)

		self.browse = Gtk.Button(label='Browse')

		self.browser_box.pack_start(self.file_path, False, True, 2)
		self.browser_box.pack_start(self.browse, False, True, 2)

		self.the_box.pack_start(self.label, False, True, 2)
		self.the_box.pack_start(self.browser_box, False, True, 2)
		self.the_box.pack_start(self.progress_label, False, True, 2)

		self.scroll = Gtk.ScrolledWindow()
		self.the_box.pack_start(self.scroll, True, True, 2)
		self.buff = StreamTextBuffer()

		self.textview = Gtk.TextView.new_with_buffer(self.buff)
		self.textview.set_border_width(5)
		self.scroll.add(self.textview)

		self.textview.connect("size-allocate", self.autoscroll)

		self.buttons = Buttons(self.file_path, self.buff)
		self.the_box.pack_start(self.buttons, False, True, 2)

		self.add(self.the_box)
		self.set_border_width(5)

		screen = Gdk.Screen.get_default()
		self.set_size_request(screen.get_width()/2, screen.get_height()/2.5)
		self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)

		self.browse.connect('clicked', self.on_browse)

	def autoscroll(self, *args):
		adj = self.scroll.get_vadjustment()
		adj.set_value(adj.get_upper() - adj.get_page_size())


	def on_browse(self, widget):
		dialog = Gtk.FileChooserDialog("Please choose a file", self,
		Gtk.FileChooserAction.OPEN,
		(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
		Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			self.file_path.set_text(dialog.get_filename())
		elif response == Gtk.ResponseType.CANCEL:
			self.file_path.set_text('')
		dialog.destroy()

installer = Installer()
installer.connect('destroy', Gtk.main_quit)
installer.show_all()
Gtk.main()


