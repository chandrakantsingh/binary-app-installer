#!/bin/bash

set -e
set +h

tarball="$1"
echo "Installing $tarball"

sudo tar xf $tarball -C /opt
sudo tee /usr/share/applications/eclipse.desktop <<"EOF"
[Desktop Entry]
Encoding=UTF-8
Name=Eclipse IDE
Comment=Java Integrated Developement Environment
GenericName=Java IDE
Exec=/opt/eclipse/eclipse %u
Terminal=false
Type=Application
Icon=eclipse
Categories=GNOME;GTK;Development
StartupNotify=true
EOF
sudo update-desktop-database

echo "Eclipse IDE installed successfully."

