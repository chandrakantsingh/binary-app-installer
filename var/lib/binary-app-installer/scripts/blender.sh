#!/bin/bash

set -e
set +h

tarball="$1"
echo "Installing $tarball"

directory=$(tar tf $tarball | cut -d/ -f1 | uniq)
sudo tar xf $tarball -C /opt
sudo ln -svf /opt/$directory /opt/blender
sudo cp -v /opt/$directory/blender.desktop /usr/share/applications/
sudo sed -i 's@Exec=blender@Exec=/opt/blender/blender@g' /usr/share/applications/blender.desktop

echo "Blender installed successfully."

