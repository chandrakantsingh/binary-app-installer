#!/bin/bash

set -e
set +h

tarball="$1"
echo "Installing $tarball"

directory=$(tar tf $tarball | cut -d/ -f1 | uniq)
sudo tar xf $tarball -C /opt
sudo ln -svf /opt/$directory /opt/jdk

sudo tee /etc/profile.d/jdk.sh<<"EOF"
export PATH=$PATH:/opt/jdk/bin
export JAVA_HOME=/opt/jdk
EOF

echo "Java Development Kit installed successfully."

