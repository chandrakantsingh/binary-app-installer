#!/bin/bash

set -e
set +h

tarball="$1"
echo "Installing $tarball"

alps install -ni kernel-headers
chmod a+x $tarball
sudo $tarball

echo "NVidia Drivers installed successfully."
