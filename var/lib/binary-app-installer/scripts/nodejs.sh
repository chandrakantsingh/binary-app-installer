#!/bin/bash

set -e
set +h

tarball="$1"
echo "Installing $tarball"

directory=$(tar tf $tarball | cut -d/ -f1 | uniq)

sudo tar xf $tarball -C /opt/
sudo ln -svf /opt/$directory /opt/nodejs

sudo tee /etc/profile.d/nodejs.sh<<"EOF"
export PATH=$PATH:/opt/nodejs/bin
EOF

echo "nodejs installed successfully."
