#!/bin/bash

set -e
set +h

tarball="$1"
echo "Installing $tarball"

directory=$(tar tf $tarball | cut -d/ -f1 | uniq)
sudo tar xf $tarball -C /opt
sudo ln -svf /opt/$directory /opt/vscode

sudo tee /usr/share/applications/vscode.desktop<<"EOF"
[Desktop Entry]
Encoding=UTF-8
Name=Microsoft Visual Studio Code
Comment=Visual Studio Code Integrated Developement Environment
GenericName=IDE
Exec=/opt/vscode/code %u
Terminal=false
Type=Application
Icon=code
Categories=GNOME;GTK;Development
StartupNotify=true
EOF

echo "Microsoft Visual Studio Code installed successfully."

