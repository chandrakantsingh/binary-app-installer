import json

root = '/var/lib/binary-app-installer/scripts'
config = '/etc/binary-app-installer.conf'

with open(config) as fp:
	conf = json.loads(fp.read())

def startsandendswith(src, start, end):
	return src.startswith(start) and src.endswith(end)

def get_script_name(file_path):
	filename = file_path.split('/')[-1]
	for entry in conf:
		print(entry)
		if startsandendswith(filename, entry['start'], entry['end']):
			return entry['script']
	return None

def get_script(the_file):
	return root + '/' + get_script_name(the_file)


